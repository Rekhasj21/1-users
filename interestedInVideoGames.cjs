function videoGamePlayers(users) {
    const players = Object.values(users).filter(({interests}) =>
        interests.toString().includes("Video Games")
    );
    return players;
}

module.exports = videoGamePlayers
