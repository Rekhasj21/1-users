function masterDegree(users) {

    const mastersUsers = Object.values(users).filter(({ qualification }) => qualification === "Masters");

    return mastersUsers
}

module.exports = masterDegree