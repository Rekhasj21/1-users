function seniorityLevel(users) {

  let usersSorted = Object.values(users).sort((a, b) => {
    const seniorityLevels = ["Senior", "Python", "Intern"];

    const aLevel = seniorityLevels.indexOf(a.desgination.split(" ")[0]);
    const bLevel = seniorityLevels.indexOf(b.desgination.split(" ")[0]);

    if (aLevel !== bLevel) {
      return aLevel - bLevel;
    }
    return b.age - a.age;
  });

  return usersSorted
}
module.exports = seniorityLevel