function groupingUserOnLanguage(users) {

  const roles = ['Python', 'Javascript', 'Golang'];

  const groupedUsers = roles.reduce((acc, role) => {
    acc[role] = Object.values(users).filter(user => user.desgination.includes(role));
    return acc;
  }, {});

  return groupedUsers
}

module.exports = groupingUserOnLanguage