function usersInGermany(users) {

  const array = Object.values(users).filter(({ nationality }) => nationality === 'Germany')
  return array
}

module.exports = usersInGermany

